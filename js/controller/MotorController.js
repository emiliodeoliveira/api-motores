class MotorController{
    constructor(){}    
    
    salvarMotor(event){
        event.preventDefault();
        console.log("Salvar Motores");
        var motor = new Motor();
        motor.nome = document.querySelector("#txtnome").value;
        motor.imagem = document.querySelector("#txtimagem").value;
        motor.descricao = document.querySelector("#txtdescricao").value;
        motor.uso = document.querySelector("#txtuso").value;
        console.log(motor);
        enviarMotor(motor);
    }
    carregarMotores() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                this.montarTabela(JSON.parse(this.responseText));
            }
        };
        xhttp.open("GET", "http://localhost:8080/api/motores", true);
        xhttp.send();
    }
    enviarMotor(motor){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 201) {
                console.log("Response recebido!");
                this.limparFormulario();
                this.carregarMotores();
            }
        };
        xhttp.open("POST", "http://localhost:8080/api/motores", true);
        xhttp.setRequestHeader("Content-Type","application/json");
        xhttp.send(JSON.stringify(motor));
    }

    limparFormulario(){
        document.querySelector("#txtnome").value="";
        document.querySelector("#txtimagem").value="";
        document.querySelector("#txtdescricao").value="";
        document.querySelector("#txtuso").value="";    
    }

    montarTabela(motores){
        var str="";
        str+= "<table>";
        str+= "<tr>";
        str+= "<th>Nome</th>";
        str+= "<th>Imagem</th>";
        str+= "<th>Descrição</th>";
        str+= "<th>Uso</th>";
        str+= "</tr>";
    
        //for(var i=0; i<motores.length; i++)
        for(var i in motores){
            str+="<tr>";
            str+="<td>"+motores[i].nome+"</td>";
            str+="<td>"+motores[i].imagem+"</td>";
            str+="<td>"+motores[i].descricao+"</td>";
            str+="<td>"+motores[i].uso+"</td>";
            str+="</tr>";
        } 
        str+= "</table>";
    
        var tabela = document.querySelector("#tabela");
        tabela.innerHTML = str;
    }
}